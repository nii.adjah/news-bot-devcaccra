package com.github.messenger4j.quickstart.boot.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.messenger4j.quickstart.boot.MessengerPlatformCallbackHandler;
import com.github.messenger4j.send.message.TemplateMessage;
import com.github.messenger4j.send.message.template.GenericTemplate;
import com.github.messenger4j.send.message.template.button.Button;
import com.github.messenger4j.send.message.template.button.UrlButton;
import com.github.messenger4j.send.message.template.common.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Utils {

    private static final Logger logger = LoggerFactory.getLogger(Utils.class);

    @Value("${news.api.url}")
    protected String NEWS_API_URL;

    @Value("${news.api.key}")
    protected String NEWS_API_KEY;

    public JsonNode getNewsByCategory(String category){
        logger.info("calling news api for news by category: "+category.toLowerCase());
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        String country = "us";
        String url = NEWS_API_URL+"top-headlines?country="+country+"&category="+category.toLowerCase()+"&apiKey="+NEWS_API_KEY;
        logger.info("calling news api with url: "+url);
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        if(response.getStatusCode() == HttpStatus.OK){
            logger.info("news api returned results, converting to json node and returning");
            ObjectMapper mapper = new ObjectMapper();
            try {
                JsonNode root = mapper.readTree(response.getBody());
                logger.info("response from news api for news by category: "+category+" : "+root.toString());
                return root;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public JsonNode getNewsBySource(String source){
        logger.info("calling news api for news by source: "+source.toLowerCase());
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        String country = "us";
        String url = NEWS_API_URL+"top-headlines?sources="+source.toLowerCase()+"&apiKey="+NEWS_API_KEY;
        logger.info("calling news api with url: "+url);
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        if(response.getStatusCode() == HttpStatus.OK){
            logger.info("news api returned results, converting to json node and returning");
            ObjectMapper mapper = new ObjectMapper();
            try {
                JsonNode root = mapper.readTree(response.getBody());
                logger.info("response from news api for news by source: "+source+" : "+root.toString());
                return root;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public JsonNode getNewsByQuery(String searchQuery){
        logger.info("calling news api for news by query: "+searchQuery.toLowerCase());
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        String url = NEWS_API_URL+"everything?q="+searchQuery.toLowerCase()+"&apiKey="+NEWS_API_KEY;
        logger.info("calling news api with url: "+url);
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        if(response.getStatusCode() == HttpStatus.OK){
            logger.info("news api returned results, converting to json node and returning");
            ObjectMapper mapper = new ObjectMapper();
            try {
                JsonNode root = mapper.readTree(response.getBody());
                logger.info("response from news api for news by query: "+searchQuery+" : "+root.toString());
                return root;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public JsonNode getTopHeadlines(){
        logger.info("calling news api for top headlines");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        String country = "us";
//        String pageSize = "8";
        String url = NEWS_API_URL+"top-headlines?country="+country+"&apiKey="+NEWS_API_KEY;
        logger.info("calling news api with url: "+url);
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        if(response.getStatusCode() == HttpStatus.OK){
            logger.info("news api returned results, converting to json node and returning");
            ObjectMapper mapper = new ObjectMapper();
            try {
                JsonNode root = mapper.readTree(response.getBody());
                logger.info("response from news api for top headlines: "+root.toString());
                return root;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public TemplateMessage convertNewsApiResultsToTemplateMessage(JsonNode newsApiResponse) {
        try{
            logger.info("convertring\n"+newsApiResponse.get("articles").toString()+" to template message");
            List<Element> elements = new ArrayList<>();
            for(JsonNode article: newsApiResponse.get("articles")){
                if(elements.size() > 9) {
                    break;
                }
                try{
                    String source = article.get("source").toString();
                    String title = article.get("title").toString();
                    logger.info("converting article: "+title+" to element");
                    String description = article.get("description").toString();
                    String url = article.get("url").toString();
                    String articleImage = article.get("urlToImage").toString();
                    List<Button> buttons = new ArrayList<>();
                    boolean articleImageIsNull = articleImage == null;
                    Button readMoreButton = UrlButton.create("Read More", new URL(url.replace("\"", "")));
                    buttons.add(readMoreButton);
                    Element newsElement = Element.create(title, Optional.of(description), articleImageIsNull ? Optional.empty() : Optional.of(new URL(articleImage.replace("\"", ""))), Optional.empty(), Optional.of(buttons));
                    elements.add(newsElement);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            GenericTemplate genericTemplate = GenericTemplate.create(elements);
            TemplateMessage templateMessage = TemplateMessage.create(genericTemplate);
            logger.info("converted all articles to template message, returning "+ elements.size() +"articles");
            return templateMessage;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return  null;
    }
}
